<?php
$this->load->view('Template/head');

if ($this->session->flashdata('error'))
{
?>

	<script>swal("Oops...", "<?php echo $this->session->flashdata('error'); ?>", "error");</script>

<?php
}

if (!$this->session->userdata('user')):
	redirect('welcome/index');
else:
$no = 1;
?>

<div class="container">
	<h1 id="h2">Pemilihan Ketua OSIS SMK Pangeran Wijayakusuma Tahun <?php echo date('Y'); ?></h1>
	<div id="rowrow" class="row">

	<?php foreach($calon->result_array() as $row): ?>

		<div class="col-md-3 col-sm-6 col-xs-6">
			<div class="panel panel-default">
				<center>
					<div class="panel-body">
						<?php echo img($row['foto'], true, ['class'=>'img-responsive img-thumbnail', 'style'=>'max-height:230px']); ?>
						<br>
						<b><?php echo $no++. ". " . $row['nama']; ?></b><p class="text-mutted"><?php echo $row['organisasi']; ?></p>
					</div>
					<div class="panel-footer">

						<a data-toggle="modal" href="#detail<?php echo $row['id']; ?>" class="btn btn-info btn-sm">
							<span class="glyphicon glyphicon-info-sign"></span> Visi dan Misi
						</a>

						<?php echo anchor('welcome/proses/'.$row['id'], '<span class="glyphicon glyphicon-ok"></span> Pilih', ['class'=>'btn btn-primary btn-sm']); ?>

					</div>
				</center>
			</div>
		</div>

		<?php endforeach; ?>

	</div> <!-- /.row -->
</div> <!-- /.container -->

<?php foreach($calon->result_array() as $row): ?>	
	<!-- Visi dan Misi -->
	<div class="modal fade" id="detail<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Visi dan Misi</h4>
				</div>
				<div class="modal-body">
					<table>
						<tr>
							<td>Nama</td>
							<td>&nbsp;</td>
							<td>:</td>
							<td>&nbsp;</td>
							<td><?php echo $row['nama']; ?></td>
						</tr>
						<tr>
							<td>Kelas</td>
							<td>&nbsp;</td>
							<td>:</td>
							<td>&nbsp;</td>
							<td><?php echo $row['kelas']; ?></td>
						</tr>
						<tr>
							<td>Organisasi</td>
							<td>&nbsp;</td>
							<td>:</td>
							<td>&nbsp;</td>
							<td><?php echo $row['organisasi']; ?></td>
						</tr>
					</table>
					<br>
					<b>VISI :</b>
						<div><?php echo htmlspecialchars_decode(html_entity_decode($row['visi'])); ?></div>
					<b>MISI :</b>
						<div><?php echo htmlspecialchars_decode(html_entity_decode($row['misi'])); ?></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div> <!-- /.modal-footer -->
			</div>
		</div>
	</div>

<?php endforeach; ?>

<?php endif; ?>

<?php $this->load->view('Template/foot'); ?>
