<?php 

// exit('Web sedang dalam perbaikan');

//header('Location: http://osis.ekoalfarisi.web.id');

$this->load->view('Template/head');

if ($this->session->flashdata('error')):
?>
	<script>swal("Oops...", "<?php echo $this->session->flashdata('error'); ?>", "error");</script>
<?php
elseif ($this->session->flashdata('success')):
?>
	<script>swal("Terima Kasih", "<?php echo $this->session->flashdata('success'); ?>", "success");</script>
<?php
endif;

if ($this->session->userdata('user')):
	redirect('welcome/pilih');
else:
?>

<div class="container">
	<div id="row" class="row">
		<div class="col-md-3 col-sm-6">

			<?php echo img('assets/img/kpo.png', true, ['id'=>'kpo', 'alt'=>'Komisi Pemilihan OSIS', 'class'=>'img-responsive']); ?>

		</div>
		<div class="col-md-3 col-sm-6">
			<p id="cara" class="pull-left">
				<b id="txt"><u>Cara Memilih :</u></b><br>
				Masukan NIS dan Password yang sudah ditentukan untuk melakukan pemilihan Ketua OSIS,
				pilih Calon Ketua OSIS dengan menekan tombol PILIH.
			</p>
		</div>
		<div class="col-md-6 col-sm-12">
			<?php
				$tahun_ini   = date('Y');
				$tambah      = mktime(0, 0, 0, date('m')+2, date('d')+7, date('Y')+1);  // angka 2,7,1 yang dicetak tebal bisa dirubah rubah
				$tahun_depan = date('Y', $tambah);
				
				echo heading('E-PILKETOS', 1); 
			?>
			<h1 id="h2">Pemilihan Ketua OSIS <br>
				SMK Wijayakusuma Tahun <?php echo $tahun_ini ."/". $tahun_depan; ?>
			</h1>
			<center>
				<div id="default" class="panel panel-default">
					<div class="panel-body">

						<?php echo form_open('welcome/login'); ?>

							<div class="form-group text-left">
								<?php
									echo form_label('Masukkan NIS', 'nis');
									$input_nis = [
										'type'        => 'number',
										'name'        => 'nis',
										'class'       => 'form-control',
										'value'       => set_value('nis'),
										'placeholder' => 'Nomor Induk Siswa',
										'autofocus'   => 'autofocus',
										'required'    => 'required'
									];
									echo form_input($input_nis);
									echo form_error('nis');
								?>
							</div>
							<div class="form-group text-left">
								<?php
									echo form_label('Password', 'password');
									$input_pass = [
										'type'        => 'password',
										'name'        => 'password',
										'class'       => 'form-control',
										'placeholder' => 'Password',
										'required'    => 'required'
									];
									echo form_input($input_pass);
									echo form_error('password');
								?>
							</div>

							<?php echo form_submit('login', 'Login', ['id'=>'primary', 'class'=>'form-control']); ?>

						<?php echo form_close(); ?>

					</div>
				</div>
			</center>
		</div>

	</div> <!-- /.row -->
</div> <!-- /.container -->

<?php endif; ?>

<?php $this->load->view('Template/foot'); ?>
