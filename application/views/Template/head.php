<?php echo doctype('html5'); ?>

<html>
<head>
<?php
$meta = array(
	array(
		'name'    => 'Content-type',
		'content' => 'text/html; charset=utf-8',
		'type'    => 'equiv'
	),
	array(
		'type'    => 'http-equiv',
		'name'    => 'X-UA-Compatible',
		'content' => 'IE=edge'
	),
	array(
		'name'    => 'viewport',
		'content' => 'width=device-width, initial-scale=1'
	),
	array(
		'name'    => 'viewport',
		'content' => 'initial-scale=1, maximum-scale=1'
	)
);
echo meta($meta);

echo "<title>" . $title . "</title>";

echo link_tag('assets/css/bootstrap.min.css');
echo link_tag('assets/css/index.css');
echo link_tag('assets/css/sweetalert.css');
echo link_tag('assets/img/icon.png', 'shortcut icon', 'image/ico');
?>
<script src="<?=base_url('assets/js/sweetalert.min.js');?>"></script>

	<style>
		#author
		{
	        color: #fff;
	    }
	</style>
</head>
<body>
	<nav id="nav" class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">  
				<a class="navbar-brand" href="#">
					<?php echo img('assets/img/ready.png', true, ['style'=>'height:100%', 'alt'=>'Brand']); ?>
				</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<?php echo img('assets/img/user_.png', true, ['style'=>'height:100%']); ?>
				</li>
				<li><br>
					<b id="txt" style="padding-top-top: 8px; color: #3c78b5;">Selamat Datang</b><br>
					<i>
					<?php
						if ($this->session->userdata('user'))
						{
							$sesi = $this->session->userdata('user');
							$row  = $this->db->get_where('data', ['nis'=>$sesi])->result();
							echo ucwords($row[0]->nama);
						}
					?>
					</i>
				</li>
			</ul>
		</div>
	</nav>	
