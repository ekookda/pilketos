<?php echo doctype('html5'); ?>

<html>
<head>

<?php
$meta = array(
	array(
		'name'    => 'Content-type',
		'content' => 'text/html; charset=utf-8',
		'type'    => 'equiv'
	),
	array(
		'type'    => 'http-equiv',
		'name'    => 'X-UA-Compatible',
		'content' => 'IE=edge'
	),
	array(
		'name'    => 'viewport',
		'content' => 'width=device-width, initial-scale=1'
	),
	array(
		'name'    => 'viewport',
		'content' => 'initial-scale=1, maximum-scale=1'
	)
);
echo meta($meta);

echo "<title>" . $title . "</title>";

echo link_tag('assets/css/bootstrap.min.css');
echo link_tag('assets/css/index.css');
echo link_tag('assets/css/sweetalert.css');
echo link_tag('assets/img/icon.png', 'shortcut icon', 'image/ico');
?>

<style type="text/css">
		body{
			background: #f1f4f7;
			padding-top: 50px;
			color: #5f6468;
		}
		/*Panels*/

		.panel {
			border: 0;
			width: 33%;
		}

		.panel-heading {
			font-size: 18px;
			font-weight: 300;
			letter-spacing: 0.025em;
			height: 66px;
			line-height: 45px;
		}

		.panel-default .panel-heading {
			background: #fff;
			border-bottom: 1px solid #eee;
			color: #5f6468;
		}

		.btn-primary{
			background-color: #30a5ff;
			border-color: #30a5ff;
		}

		.btn-primary:hover,.btn-primary:focus,.btn-primary:active{
			background: #1f9cfd;
			border-color: #1f9cfd;
		}
	</style>
</head>

<body>
	<center>
		<div class="panel panel-default">
			<div class="panel-heading">Login Admin</div>
			<div class="panel-body">

				<?php echo form_open('admin/validasi'); ?>

					<!-- Form Username -->
					<div class="form-group">
						<?php echo form_input('username', set_value('username'), ['class'=>'form-control', 'placeholder'=>'Username', 'autofocus'=>'autofocus', 'required'=>'required']); ?>
					</div>

					<!-- Form Password -->
					<div class="form-group">
						<?php echo form_password('password', set_value('password'), ['class'=>'form-control', 'placeholder'=>'Password', 'autofocus'=>'autofocus', 'required'=>'required']); ?>
					</div>

					<?php echo form_submit('login', 'Login', ['class'=>'btn btn-primary btn-block']); ?>

				<?php echo form_close(); ?>

			</div>
		</div>
	</center>

	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.1.0.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/sweetalert.min.js'); ?>"></script>

	<?php
	$flash_error   = ($this->session->flashdata('error') ? $this->session->flashdata('error') : false);
	$flash_success = ($this->session->flashdata('success') ? $this->session->flashdata('success') : false);

	if ($flash_error)
	{
	?>
		<script>sweetAlert("Oops...", "<?php echo $flash_error; ?>", "error");</script>
	<?php
	}
	elseif ($flash_success)
	{
	?>
		<script>sweetAlert("Terima Kasih", "<?php echo $flash_success; ?>", "success");</script>
	<?php
	}
	?>

</body>
</html>
