<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Laporan Hasil Pemilihan Ketua OSIS | E-Pilketos</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/icon.png'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body style="width: 100%;">
	<div class="row">
		<div class="col-md-12">
		<h4 class="text-center">Laporan Hasil Pemilihan Ketua OSIS SMK Pangeran Wijayakusuma <?php echo date('Y'); ?></h4>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>No</th>
						<th>Foto</th>
						<th>Nama</th>
						<th style="width: 70px;">Kelas</th>
						<th>Visi</th>
						<th>Misi</th>
						<th>Perolehan</th>
					</tr>
				</thead>
				<tbody>
				<?php $no=1; foreach($calon as $row):?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo img($row['foto'], FALSE, ['class'=>'img-responsive', 'width'=>'150px', 'alt'=>$row['nama']]); ?></td>
						<td><?php echo $row['nama']?></td>
						<td><?php echo $row['kelas']?></td>
						<td><?php echo htmlspecialchars_decode(html_entity_decode($row['visi'])); ?></td>
						<td><?php echo htmlspecialchars_decode(html_entity_decode($row['misi'])); ?></td>
						<td><?php echo $row['hasil']?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
<script src="<?php echo base_url('assets/js/jquery-3.1.0.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script>
	window.print();
</script>
</body>
</html>
