<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Laporan</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Laporan</h2>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Laporan Hasil Pemilihan Ketua OSIS SMK Pangeran Wijayakusuma <?php echo date('Y'); ?>
					&nbsp;&nbsp;
					<?php 
						echo form_open('report/cetak', ['style'=>'display: inline-block;', 'target'=>'_blank']);

							$button = array(
								'name'    => 'print',
								'id'      => 'print',
								'value'   => 'print',
								'type'    => 'submit',
								'content' => '<i class="glyphicon glyphicon-print"></i>Print',
								'class'   => 'btn btn-info'
							);
							echo form_button($button);

						echo form_close(); 
					?>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-condensed table-hover">
							<thead>
								<tr>
									<th class='text-center'>No</th>
									<th class='text-center'>Foto</th>
									<th class='text-center'>Nama</th>
									<th class='text-center'>Kelas</th>
									<th class='text-center'>Visi</th>
									<th class='text-center'>Misi</th>
									<th class='text-center'>Perolehan</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							$no=1; 
							if (empty($calon)):
								echo "<tr><td class='text-center' colspan='7'>" . heading('Tidak ada data yang ditampilkan.', 4) . "</td></tr>";
							else:
								foreach($calon as $row): 
							?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo img($row['foto'], FALSE, ['class'=>'img-responsive', 'height'=>'90px', 'alt'=>$row['nama']]); ?></td>
										<td><?php echo $row['nama']?></td>
										<td style="width: 70px;"><?php echo $row['kelas']?></td>
										<td><?php echo htmlspecialchars_decode(html_entity_decode($row['visi'])); ?></td>
										<td><?php echo htmlspecialchars_decode(html_entity_decode($row['misi'])); ?></td>
										<td><?php echo $row['hasil']?></td>
									</tr>
							<?php endforeach; endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
