<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo site_url('admin'); ?>">
					<svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
				</a>
			</li>
			<li class="active">Calon</li>&nbsp;&nbsp;
			<?php echo anchor('admin/add-calon', '<span class="glyphicon glyphicon-plus"></span> Tambah Calon', ['class'=>'btn btn-xs btn-primary']); ?>
			&nbsp;&nbsp;
			<?php // echo anchor('#', '<span class="glyphicon glyphicon-trash"></span> Hapus Semua', ['class'=>'btn btn-xs btn-danger', 'onclick'=>'delAll();']); ?>
			<a onclick="delAll('del-all');" href="#" class="btn btn-xs btn-danger">
				<span class="glyphicon glyphicon-trash"></span>
				Hapus Semua
			</a>&nbsp;&nbsp;
		</ol>
	</div><!--/.row-->
		
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Calon Ketua OSIS</h2>
		</div>
	</div><!--/.row-->
	
	<div class="row">

		<?php if (empty($calon)): ?>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<center>
						<div class="panel-body">
							<?php echo heading('Belum ada data !', 2); ?>
						</div>
					</center>
				</div>
			</div>
		<?php
		else:
			$no=1; 
			foreach($calon as $row):
			?>

				<div class="col-xs-6 col-md-6 col-lg-3">
					<div class="panel panel-default">
						<center>
							<div class="panel-body">
								<?php echo img(base_url($row['foto']), FALSE, ['class'=>'img-responsive img-thumbnail', 'alt'=>'kandidat '.$no, 'style'=>'max-height:220px;']); ?>
								<br>
								<b><?php echo $no++.". ".$row['nama']; ?></b><p class="text-mutted"><?=$row['organisasi'];?></p>
							</div>
							<div class="panel-footer">
								<!-- <a data-toggle="modal" href="#detail<?=$row['id']; ?>" class="btn btn-info btn-sm">Detail</a> -->
								<?php echo anchor('#detail'.$row['id'], 'Detail', ['class'=>'btn btn-info btn-sm', 'data-toggle'=>'modal']); ?>
								<?php echo anchor('admin/edit-calon/'.$row['id'], 'Edit', ['class'=>'btn btn-primary btn-sm']); ?>
								<a onclick="del(<?php echo $row['id']; ?>);" href="#" class="btn btn-sm btn-danger">
									Hapus
								</a>
								<?php // echo anchor('admin/del-calon/'.$row['id'], 'Hapus', ['class'=>'btn btn-danger btn-sm']); ?>
								<!-- <a href="?p=del_calon&id=<?=$row['id']; ?>&f=<?=$row['foto']; ?>" class="btn btn-danger btn-sm">Hapus</a> -->
							</div>
						</center>
					</div>
				</div>

			<?php endforeach; endif; ?>

	</div> <!-- /.row -->
</div> <!-- /.col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main -->

<?php foreach($calon as $row): ?>
	<!-- Detail -->
	<div class="modal fade" id="detail<?=$row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	    		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title">Visi dan Misi</h4>
	    		</div>
	    		<div class="modal-body">
    				<table>
						<tr>
							<td>Nama</td>
							<td>&nbsp;</td>
							<td>:</td>
							<td>&nbsp;</td>
							<td><?php echo $row['nama']; ?></td>
						</tr>
						<tr>
							<td>Kelas</td>
							<td>&nbsp;</td>
							<td>:</td>
							<td>&nbsp;</td>
							<td><?php echo $row['kelas']; ?></td>
						</tr>
						<tr>
							<td>Organisasi</td>
							<td>&nbsp;</td>
							<td>:</td>
							<td>&nbsp;</td>
							<td><?php echo $row['organisasi']; ?></td>
						</tr>
					</table>
					<br>
	        		<b>VISI :</b>

					<div><?php echo htmlspecialchars_decode(html_entity_decode($row['visi'])); ?></div>
	        		<b>MISI :</b>

					<div><?php echo htmlspecialchars_decode(html_entity_decode($row['misi'])); ?></div>
				</div> <!-- /.modal-body -->
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div> <!-- /.modal-footer -->
			</div> <!-- /.modal-content -->
		</div> <!-- /.modal-dialog -->
	</div> <!-- /.modal -->
<?php endforeach; ?>
