<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('admin/dashboard'); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active"><a href="<?php echo site_url('admin/calon'); ?>">Calon</a></li><li>Tambah Calon</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Tambah Calon</h2>
		</div>
	</div><!--/.row-->

<?php 
/* 
if(isset($_POST['add'])){
	$nama       = $_POST['nama'];
	$kelas      = $_POST['kelas'];
	$organisasi = $_POST['organisasi'];
	$visi       = $_POST['visi'];
	$misi       = $_POST['misi'];

	//Foto/Gambar
	$name  = $_FILES['foto']['name'];
	$asal  = $_FILES['foto']['tmp_name'];
	$error = $_FILES['foto']['error'];
	$size  = $_FILES['foto']['size'];
	$type  = $_FILES['foto']['type'];
	$file  = '../Assets/img/calon/'.$name;
	$time  = time();

	if (!empty($nama) && !empty($kelas) && !empty($organisasi) && !empty($visi) && !empty($misi) && !empty($name)) 
	{
		if( $error == 0)
		{
			if($size < 500000)
			{
				if($type == 'image/jpeg')
				{
					if(file_exists($file))
					{
						$file = str_replace(".jpg", "", $file);
						$file = $file."_".$time.".jpg";
					}
					move_uploaded_file($asal, $file);
					if (add_calon($nama, $kelas, $organisasi, $visi, $misi, $file)) 
					{
						?><script>window.location='?p=calon';</script><?php
					}else{
						?><script>swal('Oops...', 'Ada masalah saat menambahkan data', 'error');</script><?php
					}
				}else{
					?><script>swal('Oops...', 'Formatnya harus jpg', 'error');</script><?php
				}
			}else{
				?><script>swal('Oops...', 'Fotonya kegedean!! minimal 1MB', 'error');</script><?php
			}
		}else{
			?><script>swal('Oops...', 'Ada masalah saat upload foto', 'error');</script><?php
		}
	} else {
		?><script>swal('Oops...', 'Form tidak boleh kosong', 'error');</script><?php
	}
	
}
 */
?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Tambah Calon</div>
				<div class="panel-body">
				
					<?php echo validation_errors(); ?>
					<?php echo form_open_multipart('admin/store'); ?>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<?php echo form_label('Nama', 'nama', ['class'=>'control-label', 'style'=>'color:#5f6468;']); ?>
									<?php echo form_input('nama', set_value('nama'), ['class'=>'form-control', 'placeholder'=>'Nama', 'required'=>'required']); ?>
									<?php echo form_error('nama'); ?>
								</div>
								<div class="form-group">
									<?php echo form_label('Kelas', 'kelas', ['class'=>'control-label', 'style'=>'color:#5f6468;']); ?>
									<?php echo form_input('kelas', set_value('kelas'), ['class'=>'form-control', 'placeholder'=>'Kelas', 'required'=>'required']); ?>
									<?php echo form_error('kelas'); ?>
								</div>
								<div class="form-group">
									<?php echo form_label('Organisasi', 'organisasi', ['class'=>'control-label', 'style'=>'color:#5f6468;']); ?>
									<?php echo form_input('organisasi', set_value('organisasi'), ['class'=>'form-control', 'placeholder'=>'Organisasi', 'required'=>'required']); ?>
									<?php echo form_error('organisasi'); ?>
								</div>
								<div class="form-group">
									<?php echo form_label('Foto', 'foto', ['class'=>'control-label', 'style'=>'color:#5f6468;']); ?>
									<?php if ($this->session->flashdata('error_upload')): ?>
										<p class="text-danger">
											<?php echo $this->session->flashdata('error_upload'); ?>
										</p>
									<?php endif; ?>

									<input name="foto" type="file" class="form-control" onchange="loadFile1(event)"> 

									<?php
										$image_properties = array(
											'src'   => '',
											'id'	=> 'output1',
											'alt'   => 'Ukuran Foto 3x4, dengan format JPG. Ukuran file Pasfoto tidak boleh lebih dari 500 KB. Kualitas gambar harus cukup tajam dan fokus. Posisi badan dan kepala tegak sejajar menghadap kamera. Proporsi wajah antara 25% - 50% dari foto. Tidak ada bagian kepala yang terpotong dan wajah tidak boleh tertutupi ornamen. Kepala terletak di tengah secara horisontal (jarak kepala ke batas kiri kurang lebih sama dengan jarak kepala ke batas kanan)',
											'class' => 'img-responsive',
											'width' => '',
											'height'=> '300',
											'title' => 'Foto Kandidat Calon Ketua OSIS',
											'rel'   => 'lightbox'
										);
										echo img($image_properties);
									?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<?php 
										echo form_label('Visi', 'visi', ['class'=>'control-label', 'style'=>'color:#5f6468;']);

										$att_visi = array(
											'id'	=> 'visi',
											'name'	=> 'visi',
											'cols'	=> 30,
											'rows'	=> 10,
											'class'	=> 'form-control',
											'value'	=> set_value('visi'),
											// 'value'	=> htmlspecialchars_decode(html_entity_decode(set_value('visi'))),
											'placeholder' => 'Visi'
										);
										echo form_textarea($att_visi);
									?>
									<?php echo form_error('visi'); ?>
								</div>
								<div class="form-group">
									<?php
										echo form_label('Misi', 'misi', ['class'=>'control-label', 'style'=>'color:#5f6468;']);

										$att_misi = array(
											'id'	=> 'misi',
											'name'	=> 'misi',
											'cols'	=> 30,
											'rows'	=> 10,
											'class'	=> 'form-control',
											'value'	=> set_value('misi'),
											// 'value'	=> htmlspecialchars_decode(html_entity_decode(set_value('misi'))),
											'placeholder' => 'Misi'
										);
										echo form_textarea($att_misi);
									?>
									<?php echo form_error('misi'); ?>
								</div>
							</div>
						</div>

						<input type="reset" class="btn btn-default" value="Reset">
						<?php echo form_submit('tambah', 'Simpan', ['class'=>'btn btn-primary']); ?>

					<?php echo form_close(); ?>

				</div>
			</div>
		</div>
	</div>
</div>
