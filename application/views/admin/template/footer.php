
	<script src="<?php echo base_url('assets/js/jquery-3.1.0.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/tinymce/jquery.tinymce.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/tinymce/tinymce.min.js'); ?>"></script>
	<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/chartli.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.easeScroll.js'); ?>"></script>

	<?php
	$flash_error   = ($this->session->flashdata('error') ? $this->session->flashdata('error') : false);
	$error_upload  = ($this->session->flashdata('error_upload') ? $this->session->flashdata('error_upload') : false);
	$flash_success = ($this->session->flashdata('success') ? $this->session->flashdata('success') : false);

	if ($flash_error):
	?>
		<script>sweetAlert("Oops...", "<?php echo $flash_error; ?>", "error");</script>
	<?php elseif ($error_upload): ?>
		<script>sweetAlert("Oops...", "<?php echo $error_upload; ?>", "error");</script>
	<?php elseif ($flash_success): ?>
		<script>sweetAlert("Terima Kasih", "<?php echo $flash_success; ?>", "success");</script>
	<?php endif; ?>

	<script type="text/javascript">
	$(document).ready(function () {
		//Import file excel
		window.setTimeout(function() {
			$(".alert").fadeTo(1000, 0).slideUp(1000, function(){
				$(this).remove(); 
			});
		}, 5000);
	});
	</script>
	<script>
		// try {
		// 	setTimeout(function(){
		// 		console.clear();
		// 		console.log("%cWelcome!", "color: #000; font-size:45px; font-weight: bold; font-family: Arial");
		// 		console.log("%cAda perlu sesuatu di console?", "color: blue; font-size:25px; font-weight: bold; font-family: Arial");
		// 		console.log("%cJangan paste disini, code yang tidak Anda mengerti.", "color: red; font-size:20px; font-weight: bold; font-family: Arial");
		// }, 1000);
		// } catch(e) {

		// }
	</script>
	<script> 
		//easeScroll
		$("html").easeScroll();

		//Import
		$(document).ready(function(){
			// Sembunyikan alert validasi kosong
			$("#kosong").hide();
		});

		function del(id){
			swal({
			    title: "Are you sure?",
				text: "Data akan di hapus",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function(){ 
				window.location="<?php echo site_url('admin/del-calon/'); ?>" + id;
			}); // Delete data pemilih ???
		}

		function delAll(url)
		{
			swal({
			    title: "Are you sure?",
				text: "Data akan di hapus",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function(){ 
				window.location="<?php echo site_url('admin/'); ?>" + url;
			}); // Delete data pemilih ???
		}

		//Table
		$(function() { $('#tabel').dataTable(); });

		// Preview 1
		var loadFile1 = function(event) {
			var reader = new FileReader();
				reader.onload = function(){
					var output1 = document.getElementById('output1');
					output1.src = reader.result;
				};
			reader.readAsDataURL(event.target.files[0]);
		};

		// Preview 2
		var loadFile2 = function(event) {
			var reader = new FileReader();
				reader.onload = function(){
					var output2 = document.getElementById('output2');
					output2.src = reader.result;
				};
			reader.readAsDataURL(event.target.files[0]);
		};

		//Tambah calon
		tinymce.init({
			selector:"#visi",
			menubar: false,
		    plugins: ["preview wordcount advlist autolink lists link image charmap preview pagebreak searchreplace insertdatetime, fullscreen hr , table,  directionality, emoticons, textcolor, colorpicker, textpattern, code"],
		    toolbar: "undo redo | fontselect fontsizeselect | styleselect | bullist numlist | forecolor backcolor emoticons | preview wordcount",
		    convert_urls: false,
		    theme_advanced_font_sizes : "8px,10px,12px,14px,16px,18px,20px,24px,32px,36px",
		});

		tinymce.init({
			selector:"#misi",
			menubar: false,
		    plugins: ["preview wordcount advlist autolink lists link image charmap preview pagebreak searchreplace insertdatetime, fullscreen hr , table,  directionality, emoticons, textcolor, colorpicker, textpattern, code"],
		    toolbar: "undo redo | fontselect fontsizeselect | styleselect | bullist numlist | forecolor backcolor emoticons | preview wordcount",
		    convert_urls: false,
		    theme_advanced_font_sizes : "8px,10px,12px,14px,16px,18px,20px,24px,32px,36px",
		});

		//Style
		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
			if ($(window).width() > 768) $('#collapse').collapse('show')
		});
		$(window).on('resize', function () {
			if ($(window).width() <= 767) $('#collapse').collapse('hide')
		});
	</script>	

	<!-- column chart -->
	<script>
		var chart = echarts.init(document.getElementById('chart'));
		window.onresize = chart.resize
		option = 
		{
		    tooltip: { trigger: 'item'},
		    toolbox: { show: true, feature: { saveAsImage: { show: true } } },

		    calculable: true,
		    grid: { borderWidth: 0, y: 35, y2: 35, x: 30, x2: 30 },
		    xAxis: [  
		        { type: 'category', show: true,
		            <?php //$hasil = laporan(); ?>
		            data: [ <?php foreach($hasil as $row): echo "'".$row['nama']."',"; endforeach; ?> ]
		        }
		    ],

		    yAxis: [ { type: 'value', show: true } ],
		    series: [
		        { name: '', type: 'bar', 
		        	itemStyle: {
		            	normal: {
		                    color: function (params) {
		                        // build a color map as your need.
		                        var colorList = [
		                        	'#27ae60', '#2980b9', '#8e44ad', '#2c3e50', 
		                        	'#d35400', '#c0392b', '#bdc3c7', '#7f8c8d' 
		                        ];
		                        return colorList[params.dataIndex]
		                    },
		                    label: { show: true, position: 'top', formatter: '{b}\n{c}' }
		                }
		            },
		            data: [ <?php foreach($hasil as $row): echo "'".$row['hasil']."',"; endforeach; ?> ],
		        }
		    ]
		};

		chart.setOption(option);
	</script>
</body>
</html>
