<?php
if(!$this->session->userdata('admin')):
	redirect('admin/login');
endif;

echo doctype('html5');
?>

<html>
<head>

<?php
$meta = array(
	array(
		'name'    => 'Content-type',
		'content' => 'text/html; charset=utf-8',
		'type'    => 'equiv'
	),
	array(
		'type'    => 'http-equiv',
		'name'    => 'X-UA-Compatible',
		'content' => 'IE=edge'
	),
	array(
		'name'    => 'viewport',
		'content' => 'width=device-width, initial-scale=1'
	),
	array(
		'name'    => 'viewport',
		'content' => 'initial-scale=1, maximum-scale=1'
	)
);
echo meta($meta);

echo "<title>" . $title . "</title>";

echo link_tag('assets/css/bootstrap.min.css');
echo link_tag('assets/css/index.css');
echo link_tag('assets/css/sweetalert.css');
echo link_tag('assets/css/styles.css');
echo link_tag('assets/css/dataTables.bootstrap.css');
echo link_tag('assets/img/icon.png', 'shortcut icon', 'image/ico');
?>

<script src="<?php echo base_url('assets/js/sweetalert.min.js'); ?>"></script>
<!--Icons-->
<script src="<?php echo base_url('assets/js/lumino.glyphs.js'); ?>"></script>

</head>
<body>

<?php if ($this->session->flashdata('success')): ?>
	<script>sweetAlert("Sukses", "<?php echo $this->session->flashdata('success') ?>", "success");</script>
<?php endif; ?>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url('admin/dashboard'); ?>"><span>E-Pilketos </span>Admin</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
							<?php
							foreach ($tampil as $row):
								$nama = $row['nama'];
							endforeach;
							echo $nama;
							?> 
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo site_url('admin/profil'); ?>">
									<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
									Profil
								</a>
							</li>
							<li role="presentation" class="divider"></li>
							<li>
								<a href="<?php echo site_url('admin/dashboard'); ?>">
									<svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>
									Dashboard
								</a>
							</li>
							<li>
								<a href="<?php echo site_url('admin/calon'); ?>">
									<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
									Calon
								</a>
							</li>
							<li>
								<a href="<?php echo site_url('report/data'); ?>">
									<svg class="glyph stroked table"><use xlink:href="#stroked-table"/></svg>
									Data Pemilih
								</a>
							</li>
							<li>
								<a href="<?php echo site_url('report/laporan'); ?>">
									<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg>
									Laporan
								</a>
							</li><br>
							<li>
								<a href="<?php echo site_url(); ?>" target="_blank">
									<svg class="glyph stroked desktop"><use xlink:href="#stroked-desktop"/></svg>
									Kunjungi Situs
								</a>
							</li>
							<li role="presentation" class="divider"></li>
							<li>
								<a href="<?php echo site_url('admin/logout'); ?>">
									<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> 
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="collapse" class="col-sm-3 col-lg-2 sidebar">
		<hr>
		<ul class="nav menu">
			<li>
				<a href="<?php echo site_url('admin/dashboard'); ?>">
					<svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>
					Dashboard
				</a>
			</li>
			<li>
				<a href="<?php echo site_url('admin/calon'); ?>">
					<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
					Calon
				</a>
			</li>
			<li>
				<a href="<?php echo site_url('report/data'); ?>">
					<svg class="glyph stroked table"><use xlink:href="#stroked-table"/></svg>
					Data Pemilih
				</a>
			</li>
			<li>
				<a href="<?php echo site_url('report/laporan'); ?>">
					<svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg>
					Laporan
				</a>
			</li><br>
			<li>
				<a href="<?php echo site_url(); ?>" target="_blank">
					<svg class="glyph stroked desktop"><use xlink:href="#stroked-desktop"/></svg>
					Kunjungi Situs
				</a>
			</li>
			<li role="presentation" class="divider"></li>
			<li>
				<a href="<?php echo site_url('admin/logout'); ?>">
					<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> 
					Logout
				</a>
			</li>
		</ul>

	</div><!--/.sidebar-->
