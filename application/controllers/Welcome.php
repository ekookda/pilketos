<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	var $title = "E-VOTES SMK PANGERAN WIJAYAKUSUMA";

	public function __construct()
	{
		parent::__construct();
		$this->load->model('backend_model');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$this->load->view('welcome_message', $data);
	}

	public function login()
	{
		if (!$this->input->post('login'))
		{
			$this->session->set_flashdata('error', 'Anda belum login');
			redirect('welcome');
		}
		else
		{
			$pesanError = array(
				'required' => '<span style="color:red;">%s wajib diisi</span>'
			);

			// Validasi
			$rule = array(
				array(
					'field' => 'nis',
					'label' => 'NIS',
					'rules' => 'required|min_length[6]|max_length[6]|numeric',
				),
				array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'required'
				)
			);
			$this->form_validation->set_rules($rule);

			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error', 'Periksa kembali form !');
				$this->index();
			}
			else 
			{
				$nis  = $this->input->post('nis');
				$pass = $this->input->post('password');

				$query = $this->backend_model->get_data('data', ['nis'=>$nis]);

				if ($query->num_rows() == 1)
				{
					$row = $query->result();
					// if (password_verify($pass, $row[0]->password))
					if (MD5($pass) == $row[0]->password)
					{
						$this->session->set_userdata('user', $nis);
						redirect('welcome/pilih');
					}
					else
					{
						$this->session->set_flashdata('error', 'Password tidak cocok !');
						redirect('welcome');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Data tidak ditemukan !');
					redirect('welcome');
				}
			}
		}
	}

	public function logout()
	{
		if ($this->session->unset_userdata(['user', 'isLoggedIn']))
		{
			return true;
		}

		return false;
	}

	public function pilih()
	{
		// $this->load->helper('calon');

		$query = $this->backend_model->get_all('calon');

		$data['title'] = $this->title;
		$data['calon'] = $query;

		$this->load->view('pilih', $data);
	}

	public function proses($id)
	{
		// cek status pilih
		$nis    = $this->session->userdata('user');
		$qry    = $this->backend_model->query("SELECT status FROM data WHERE nis='$nis'")->result();
		$status = $qry[0]->status;

		if ($status == 0)
		{
			// Hak pilih sudah digunakan / Tidak bisa memilih lebih dari 1x
			$this->logout();
			$this->session->set_flashdata('error', 'Hak pilih anda sudah digunakan');
			redirect('welcome');
		}
		else
		{
			// Hak pilih belum digunakan
			$calon   = $this->backend_model->query("SELECT nama, hasil FROM calon WHERE id='$id'");
			$row     = $calon->result_array();
			$tambah  = ($row[0]['hasil']) + 1;
			$sesi    = ($this->session->userdata('user') ? $this->session->userdata('user') : false);
			$memilih = $this->backend_model->pilih($sesi, $id, $tambah);

			if ($memilih == true)
			{
				$this->logout();
				$this->session->set_flashdata('success', 'Anda sudah memilih !');
				redirect('welcome');
			}
		}
	}

}

/* End of file Welcome.php */
