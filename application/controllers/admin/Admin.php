<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller
{

	var $title = "Dashboard | E-Pilketos";

	public function __construct()
	{
		parent::__construct();
		$this->load->library('template');
		$this->load->model('backend_model');
	}

	public function is_admin()
	{
		$sess_admin = ($this->session->userdata('isLoggedIn') ? $this->session->userdata('isLoggedIn') : NULL);

		if ($sess_admin != 'admin')
		{
			$this->session->set_flashdata('error', 'Anda belum melakukan login !'); 
			redirect('admin/login'); 
		}
	}

	public function login()
	{
		$data['title'] = $this->title;
		$this->load->view('admin/login', $data);
	}

	public function process_login()
	{
		if ($this->input->post('login'))
		{
			// Validasi Form
			$rule = array(
				array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'required'
				),
				array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'required|min_length[5]'
				)
			);
			$this->form_validation->set_rules($rule);

			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error', 'Periksa kembali form !');
				$this->login();
			}
			else
			{
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				$query = $this->backend_model->get_data('admin', ['user'=>$username]);

				if ($query->num_rows() == 1)
				{
					$row = $query->result();
					if (password_verify($password, $row[0]->pass))
					{
						$this->session->set_userdata(['admin'=>$username, 'id'=>$row[0]->id, 'isLoggedIn'=>'admin']);
						$this->session->set_flashdata('success', 'Selamat datang, '. $row[0]->nama .' !');
						redirect('admin/dashboard');
					}
					else
					{
						$this->session->set_flashdata('error', 'Password tidak cocok !');
						redirect('admin/login');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Data tidak ditemukan !');
					redirect('admin/login');
				}
			}
			
		}
		else
		{
			$this->session->set_flashdata('error', 'Anda belum melakukan login !');
			redirect('admin/login');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata(['user', 'isLoggedIn']);
		$this->session->set_flashdata('success', 'Anda telah logout !');

		redirect('admin/login');
	}

	public function index()
	{
		$this->is_admin();

		$data['tampil'] = $this->tampil()->result_array();
		$data['hasil']  = $this->hasil()->result_array();
		$data['sudah']  = $this->sudah_pilih();
		$data['belum']  = $this->belum_memilih();
		$data['jumlah'] = $this->jumlah_pemilih();
		$data['title']  = $this->title;

		$this->template->view('admin/dashboard', $data);
	}

	public function tampil()
	{
		$this->is_admin();
		$sesi = ($this->session->userdata('admin') ? $this->session->userdata('admin') : false);
		return $this->backend_model->get_data('admin', ['user'=>$sesi]);
	}

	public function hasil()
	{
		$this->is_admin();
		return $this->backend_model->get_all('calon');
	}

	public function belum_memilih()
	{
		$this->is_admin();
		return $this->backend_model->get_data('data', ['status'=>1])->num_rows();
	}

	public function sudah_pilih()
	{
		$this->is_admin();
		return $this->backend_model->get_data('data', ['status'=>0])->num_rows();
	}

	public function jumlah_pemilih()
	{
		$this->is_admin();
		return $this->backend_model->get_all('data')->num_rows();
	}

	/* Calon */
	public function data_calon()
	{
		$this->is_admin();

		$data['tampil'] = $this->tampil()->result_array();
		$data['hasil']  = $this->hasil()->result_array();
		$data['sudah']  = $this->sudah_pilih();
		$data['belum']  = $this->belum_memilih();
		$data['jumlah'] = $this->jumlah_pemilih();

		return $data;
	}

	public function calon()
	{
		$this->is_admin();

		$data['tampil'] = $this->tampil()->result_array();
		$data['hasil']  = $this->hasil()->result_array();
		$data['calon']  = $this->hasil()->result_array();
		$data['sudah']  = $this->sudah_pilih();
		$data['belum']  = $this->belum_memilih();
		$data['jumlah'] = $this->jumlah_pemilih();
		$data['title']  = $this->title;

		$this->template->view('admin/calon/index', $data);
	}

	public function add_calon()
	{
		$this->is_admin();

		$data['tampil'] = $this->tampil()->result_array();
		$data['hasil']  = $this->hasil()->result_array();
		$data['sudah']  = $this->sudah_pilih();
		$data['belum']  = $this->belum_memilih();
		$data['jumlah'] = $this->jumlah_pemilih();
		$data['title']  = $this->title;

		$this->template->view('admin/calon/add_calon', $data);
	}

	public function store_calon()
	{
		if ($this->input->post('tambah'))
		{
			$this->is_admin();

			// validasi form input/multipart
			$rules = array(
				array(
					'field' => 'nama',
					'label' => 'Nama',
					'rules' => 'required'
				),
				array(
					'field' => 'kelas',
					'label' => 'Kelas',
					'rules' => 'required'
				),
				array(
					'field' => 'organisasi',
					'label' => 'Organisasi',
					'rules' => 'required'
				),
				array(
					'field' => 'visi',
					'label' => 'Visi',
					'rules' => 'required'
				),
				array(
					'field' => 'misi',
					'label' => 'Misi',
					'rules' => 'required'
				)
			);
			$this->form_validation->set_rules($rules);
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

			if ($this->form_validation->run() == FALSE)
			{
				// validasi form gagal
				$this->session->set_flashdata('error', 'Periksa kembali form !');
				$this->add_calon();
				// redirect('admin/add-calon');
			}
			else
			{
				// Ambil data dari form
				$nama       = ucwords($this->input->post('nama'));
				$kelas      = strtoupper($this->input->post('kelas'));
				$organisasi = strtoupper($this->input->post('organisasi'));
				$visi       = $this->input->post('visi');
				$misi       = $this->input->post('misi');

				// Setting Preferences Upload
				$config['upload_path']   = './assets/img/calon/';
				$config['allowed_types'] = 'png|jpeg|jpg';
				$config['max_size']      = 2048;
				$config['max_width']     = 480;
				$config['max_height']    = 680;
				$config['file_name']     = $_FILES['foto']['name'];
				$config['file_temp']     = $_FILES['foto']['tmp_name'];
				$config['error']         = $_FILES['foto']['error'];
				$config['file_size']     = $_FILES['foto']['size'];
				$config['file_type']     = $_FILES['foto']['type'];
				$config['remove_space']  = true;
				$config['overwrite']     = true;

				$file  = $config['upload_path'].$_FILES['foto']['name'];
				$time  = time();

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('foto'))
				{
					$error = array('error' => $this->upload->display_errors('<span class="text-danger">', '</span>'));
					$this->session->set_flashdata('error_upload', "<div style='color:#ff0000;'>" . $error['error'] . "</div>");
					$this->add_calon();
				}
				else
				{
					// Upload File
					$data = ['upload_data' => $this->upload->data()];

					// move_uploaded_file($config['file_temp'], $file);

					$data =  array(
						'nama'       => $nama,
						'kelas'      => $kelas,
						'organisasi' => $organisasi,
						'visi'       => $visi,
						'misi'       => $misi,
						'foto'       => $file,
						'hasil'      => 0
					);
					$insert = $this->backend_model->set_data('calon', $data);

					if ($insert)
					{
						$this->session->set_flashdata('success', 'Tambah data Berhasil !');
						redirect('admin/calon');
					}
					else
					{
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_upload', $error['error']);
						redirect('admin/add-calon');
					}
				}
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Anda belum mengisi data !');
			redirect('admin/add-calon');
		}
	}

	public function edit_calon($id)
	{
		$this->is_admin();

		$id_calon = $this->backend_model->get_data('calon', ['id'=>$id]);

		$data['tampil'] = $this->tampil()->result_array();
		$data['hasil']  = $this->hasil()->result_array();
		$data['sudah']  = $this->sudah_pilih();
		$data['belum']  = $this->belum_memilih();
		$data['jumlah'] = $this->jumlah_pemilih();
		$data['title']  = $this->title;

		$data['id_calon'] = $id_calon->result_array();

		$this->template->view('admin/calon/edit_calon', $data);
	}

	public function form_upload()
	{
		if ($this->input->post('edit'))
		{
			$this->is_admin();

			$id = $this->input->post('id');

			// validasi form input/multipart
			$rules = array(
				array(
					'field' => 'nama',
					'label' => 'Nama',
					'rules' => 'required'
				),
				array(
					'field' => 'kelas',
					'label' => 'Kelas',
					'rules' => 'required'
				),
				array(
					'field' => 'organisasi',
					'label' => 'Organisasi',
					'rules' => 'required'
				),
				array(
					'field' => 'visi',
					'label' => 'Visi',
					'rules' => 'required'
				),
				array(
					'field' => 'misi',
					'label' => 'Misi',
					'rules' => 'required'
				)
			);
			$this->form_validation->set_rules($rules);
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

			if ($this->form_validation->run() == FALSE)
			{
				// validasi form gagal
				$this->session->set_flashdata('error', 'Periksa kembali form !');
				$this->edit_calon($id);
				// redirect('admin/edit-calon/'.$id);
			}
			else
			{
				// Ambil data dari form
				$data_form = array(
					'nama'       => $this->input->post('nama'),
					'kelas'      => $this->input->post('kelas'),
					'organisasi' => $this->input->post('organisasi'),
					'visi'       => $this->input->post('visi'),
					'misi'       => $this->input->post('misi')
				);

				if ($_FILES && $_FILES['foto']['name'])
				{
					// Setting Preferences Upload
					$config['upload_path']   = './assets/img/calon/';
					$config['allowed_types'] = 'jpeg|jpg|png';
					$config['max_size']      = 2048;
					$config['max_width']     = 480;
					$config['max_height']    = 680;
					$config['file_name']     = $_FILES['foto']['name'];
					$config['file_temp']     = $_FILES['foto']['tmp_name'];
					$config['error']         = $_FILES['foto']['error'];
					$config['file_size']     = $_FILES['foto']['size'];
					$config['file_type']     = $_FILES['foto']['type'];
					$config['remove_space']  = true;
					$config['overwrite']     = true;

					$file  = './assets/img/calon/'.$_FILES['foto']['name'];
					$time  = time();

					$this->load->library('upload', $config);
					
					if (!$this->upload->do_upload('foto'))
					{
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_upload', "<div style='color:#ff0000;'>" . $error['error'] . "</div>");
						$this->edit_calon($id);
					}
					else
					{
						$qry  = $this->backend_model->get_data('calon', ['id'=>$id])->result_array();

						$data_form = array(
							'nama'       => $this->input->post('nama'),
							'kelas'      => $this->input->post('kelas'),
							'organisasi' => $this->input->post('organisasi'),
							'visi'       => $this->input->post('visi'),
							'misi'       => $this->input->post('misi'),
							'foto'       => $file
						);

						// Remove old image in folder 'img' using unlink()
						unlink($qry[0]['foto']);

						// Upload File
						$data = ['upload_data' => $this->upload->data()];

						move_uploaded_file($config['file_temp'], $file);

						$update = $this->backend_model->set_update('calon', $data_form, ['id'=>$id]);
						// $update = $this->backend_model->query("UPDATE calon SET nama='$nama', kelas='$kelas', organisasi='$organisasi', 
																// visi='$visi', misi='$misi', foto='$file' WHERE id='$id'");

						if ($update)
						{
							$this->session->set_flashdata('success', 'Edit data Berhasil !');
							redirect('admin/calon');
						}
						else
						{
							$error = array('error' => $this->upload->display_errors());
							$this->session->set_flashdata('error_upload', $error['error']);
							$this->edit_calon($id);
						}
					}
				}
				else
				{
					// Do this if there's no image file uploaded (No file upload)
					$update = $this->backend_model->set_update('calon', $data_form, ['id'=>$id]);

					if ($update)
					{
						$this->session->set_flashdata('success', 'Edit data berhasil !');
						redirect('admin/calon');
					}
					else
					{
						$this->session->set_flashdata('error_upload', 'Edit gagal !');
						$this->edit_calon($id);
					}
				}
			}
		}
		else
		{
			// tidak menekan tombol edit/submit
			$this->session->set_flashdata('error', 'Edit gagal !');
			redirect('admin/calon');
		}
	}

	public function del_calon($id)
	{
		$this->is_admin();

		$del = $this->backend_model->del_data('calon', ['id'=>$id]);
		
		if ($del)
		{
			$this->session->set_flashdata('success', 'Hapus data berhasil !');
			redirect('admin/calon');
		}
		else
		{
			$this->session->set_flashdata('error_upload', 'Gagal menghapus !');
			redirect('admin/calon');
		}

		return false;
	}

	public function del_all_calon()
	{
		$this->is_admin();

		$empty = $this->backend_model->empty_table('calon');

		if ($empty)
		{
			$this->session->set_flashdata('success', 'Hapus data berhasil !');
			redirect('admin/calon');
		}
		else
		{
			$this->session->set_flashdata('error_upload', 'Gagal menghapus !');
			redirect('admin/calon');
		}

		return false;
	}

	public function del_all_pemilih()
	{
		$this->is_admin();

		$empty = $this->backend_model->empty_table('data');

		if ($empty)
		{
			$this->session->set_flashdata('success', 'Hapus data berhasil !');
			redirect('report/data');
		}
		else
		{
			$this->session->set_flashdata('error_upload', 'Gagal menghapus !');
			redirect('report/data');
		}

		return false;
	}

	/* Report or Laporan */
	public function report_data()
	{
		$this->is_admin();

		$data['tampil_data'] = $this->backend_model->get_all('data')->result_array();
		$data['tampil']      = $this->tampil()->result_array();
		$data['hasil']       = $this->hasil()->result_array();
		$data['sudah']       = $this->sudah_pilih();
		$data['belum']       = $this->belum_memilih();
		$data['jumlah']      = $this->jumlah_pemilih();
		$data['title']       = $this->title;

		$this->template->view('admin/report/data', $data);
	}

	public function import()
	{
		if ($this->input->post('import'))
		{
			$this->is_admin();

			$config = array(
				'upload_path'   => './assets/Format/upload/',
				'allowed_types' => 'xls|xlsx|csv|ods|ots',
				'max_size'      => 10000,
				'file_name'     => $_FILES['file']['name'],
				'overwrite'		=> true,
				'remove_spaces'	=> true
			);

			$this->load->library('upload', $config);
			$this->load->library(['PHPExcel','PHPExcel/IOFactory']);
			$this->load->helper('file');

			if ($this->upload->do_upload('file'))
			{
				// print_r($this->upload->data());
				$media = $this->upload->data();
				$inputFileName = $media['file_path'].$media['file_name'];

				try
				{
					// $objPHPExcel = IOFactory::load($inputFileName);
					$inputFileType = IOFactory::identify($inputFileName);
					$objReader     = IOFactory::createReader($inputFileType);
					$objPHPExcel   = $objReader->load($inputFileName);
				}
				catch (Exception $e)
				{
					die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '" : ' . $e->getMessage());
				}

				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
				$arrayCount     = count($allDataInSheet); // Here get total count of row in that Excel sheet
				// $sheet         = $objPHPExcel->getSheet(0);
				// $highestRow    = $sheet->getHighestRow();
				// $highestColumn = $sheet->getHighestColumn();

				for($i = 2; $i <= $arrayCount; $i++):
					$a = $allDataInSheet[$i]["A"];
					// $b = substr($allDataInSheet[$i]["B"], 2);
					$b = $allDataInSheet[$i]["B"];
					$c = $allDataInSheet[$i]["C"];
					$d = $allDataInSheet[$i]["D"];
					$e = $allDataInSheet[$i]["E"];

					// password hash
					$b = MD5($b);

					$insert_data = array(
						'nis'      => $a,
						'password' => $b,
						'nama'     => $c,
						'kelas'    => $d,
						'jk'       => $e,
						'status'   => 1
					);

					$hasils = $this->backend_model->set_data('data', $insert_data);
					// $query  = "INSERT INTO data (nis,password,nama,kelas,jk,status) VALUES('$a','$b','$c','$d','$e','1')";
					// $hasils = $this->backend_model->query($query);
				endfor;
				
				if ($hasils)
				{
					@unlink ("$inputFileName");
					// header("location:?p=data&msg=send");
					$this->session->set_flashdata('success', 'Import data berhasil !');
					redirect('report/data');
				}
				else
				{
					$this->session->set_flashdata('error', 'Gagal mengimport data !');
					redirect('report/data');
				}
			}
			else
			{
				// print_r($this->upload->display_errors());
				$error = ['error'=>$this->upload->display_errors()];
				$this->session->set_flashdata('error', $error);
				$this->report_data();
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Anda belum melakukan import data !');
			redirect('report/data');
		}
	}

	public function report_laporan()
	{
		$this->is_admin();

		$data['hasil']  = $this->hasil()->result_array();
		$data['tampil'] = $this->tampil()->result_array();
		$data['calon']  = $this->hasil()->result_array();
		$data['title']  = $this->title;

		$this->template->view('admin/report/laporan', $data);
	}

	public function report_print()
	{
		$this->is_admin();

		$data['hasil']  = $this->hasil()->result_array();
		$data['tampil'] = $this->tampil()->result_array();
		$data['calon']  = $this->hasil()->result_array();

		$this->load->view('admin/report/print', $data);
	}

}

/* End of file Admin.php */
/* Location file controller/admin/admin/Admin.php */
