<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']   = 'welcome';
$route['404_override']         = '';
$route['translate_uri_dashes'] = TRUE;

// URI Halaman Admin
$route['admin']                   = "admin/admin";
$route['admin/login']             = "admin/admin/login";
$route['admin/logout']            = "admin/admin/logout";
$route['admin/validasi']          = "admin/admin/process_login";
$route['admin/dashboard']         = "admin/admin/index";
$route['admin/calon']             = "admin/admin/calon";
$route['admin/add-calon']         = "admin/admin/add_calon";        // view form tambah calon
$route['admin/store']             = "admin/admin/store_calon";      // view simpan data calon
$route['admin/edit-calon/(:num)'] = "admin/admin/edit_calon/$1";
$route['admin/del-calon/(:num)']  = "admin/admin/del_calon/$1";
$route['admin/del-all']           = "admin/admin/del_all_calon";
$route['admin/del_all_pemilih']   = "admin/admin/del_all_pemilih";
$route['admin/form_upload']       = "admin/admin/form_upload";
$route['report/data']             = "admin/admin/report_data";
$route['report/format']           = "admin/admin/report_format";
$route['report/import']           = "admin/admin/import";
$route['report/laporan']          = "admin/admin/report_laporan";
$route['report/cetak']            = "admin/admin/report_print";
