<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Template
{
	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function view($content, $data = NULL)
	{
		/*
		* $data['headernya'] , $data['contentnya'] , $data['footernya']
		* variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
		*/
		$data['header']  = $this->CI->load->view('admin/template/header', $data, TRUE);
		$data['content'] = $this->CI->load->view($content, $data, TRUE);
		$data['footer']  = $this->CI->load->view('admin/template/footer', $data, TRUE);

		$this->CI->load->view('admin/template/index', $data);
	}

}

/* End of file Template.php */
