<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_model extends CI_Model
{

	public function get_all($table)
	{
		$query = $this->db->get($table);

		return $query;
	}

	public function get_data($table, $where=array())
	{
		$query = $this->db->get_where($table, $where);

		return $query;
	}

	public function set_data($table, $data=[])
	{
		$this->db->insert($table, $data);

		return $this->db->insert_id();
	}

	public function set_update($table, $data=[], $where=[])
	{
		$this->db->where($where);
		$query = $this->db->update($table, $data);

		return $query;
	}

	public function query($query)
	{
		$qry = $this->db->query($query);

		return $qry;
	}

	public function pilih($sesi, $id, $tambah)
	{
		if ($this->db->update('data', ['status'=>0], ['nis'=>$sesi]))
		{
			// update hasil
			return $this->db->update('calon', ['hasil'=>$tambah], ['id'=>$id]);

			// return true;
		}

		return false;
	}

	public function del_data($table, $where=[])
	{
		$this->db->where($where);
		$delete = $this->db->delete($table);

		return $delete;
	}

	public function empty_table($table)
	{
		return $this->db->truncate($table);
	}

}

/* End of file Backend_model.php */
